// 1. Soal No. 1 (Membuat kalimat)
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh);

console.log();
// 2. Soal No.2 Mengurai kalimat (Akses karakter dalam string)
var sentence = "I am going to be React Native Developer"; 
var a = sentence.split(" ");

console.log('First Word: ' + a[0]); 
console.log('Second Word: ' + a[1]); 
console.log('Third Word: ' + a[2]); 
console.log('Fourth Word: ' + a[3]); 
console.log('Fifth Word: ' + a[4]); 
console.log('Sixth Word: ' + a[5]); 
console.log('Seventh Word: ' + a[6]); 
console.log('Eighth Word: ' + a[7])

console.log();
// 2. Soal No. 3 Mengurai Kalimat (Substring)
var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' +secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);

console.log();
// 4. Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool'; 
var exampleFirstWord3 = sentence2.substring(0, 3);
var secondWord3 = sentence2.substring(4, 14);
var thirdWord3 = sentence2.substring(15, 17);
var fourthWord3 = sentence2.substring(18, 20);
var fifthWord3 = sentence2.substring(21, 25);

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength);
console.log('Second Word: ' +secondWord3 + ', with length: ' + secondWordLength);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);