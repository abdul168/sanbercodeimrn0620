//1. if-else

var nama = "Jhon";
var peran = "";

if (nama!=""){
    if (peran!=""){
        if(peran=="Penyihir"){
            console.log("Selamat datang di Dunia Werewolf, "+nama);
            console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
        }else if (peran=="Guard"){
            console.log("Selamat datang di Dunia Werewolf, "+nama);
            console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
        }else if (peran=="Werewolf"){
            console.log("Selamat datang di Dunia Werewolf, "+nama);
            console.log("Halo "+peran+" "+nama+", kamu akan memakan mangsa setiap malam!");
        }else{
            console.log("Peran tidak ada!");
        }
    }else{
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
    }
}else{
    console.log("Nama harus diisi!");
}

//2. Switch Case

var hari = 3; 
var bulan = 12; 
var tahun = 1945;


if(hari>0 && hari<=31){
    hari = hari;
}else{
    hari="hari tidak valid! harus diantara angka (1 - 31)";
    return console.log(hari);
}


if(bulan>0 && bulan<=12){
    switch(bulan){
        case 1 : bulan="Januari";break;
        case 2 : bulan="Februari";break;
        case 3 : bulan="Maret";break;
        case 4 : bulan="April";break;
        case 5 : bulan="Mei";break;
        case 6 : bulan="Juni";break;
        case 7 : bulan="Juli";break;
        case 8 : bulan="Agustus";break;
        case 9 : bulan="September";break;
        case 10 : bulan="Oktober";break;
        case 11 : bulan="November";break;
        case 12 : bulan="Desember";break;
        default : bulan="";
    }
}else{
    bulan="bulan tidak valid! harus diantara angka (1 - 12)";
    return console.log(bulan);
}

if(tahun>=1900 && tahun<=2200){
    tahun=tahun;
}else{
    tahun="tahun tidak valid! harus diantara angka (1900 - 2200)";
    return console.log(tahun);
}

console.log (hari+" "+bulan+" "+tahun);