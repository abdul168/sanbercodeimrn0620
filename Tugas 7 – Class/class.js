console.log("=== 1. Animal Class ===");
class Animal {
    constructor (name){
        this.name = name;
    }
    get legs(){
        return 4;
    }
    set legs (x){
        this.legs = x;
    }
    get cold_blooded(){
        return false;
    }
    set cold_blooded (x){
        this.cold_blooded = x;
    }
}
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor(brand) {
      super(brand);
      this.Ape = brand;
    }
    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(brand) {
      super(brand);
      this.Frog = brand;
    }
    jump() {
        console.log("hop hop");
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


console.log();
console.log("=== 2. Function to Class ===")
class Clock {
    constructor ({template}){
        this.template = template;
    }

    timer (){
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop () {
        clearInterval(this.timer);
    }

    start () {
        this.render();
        var a = this;
        this.timer = setInterval(function(){
            a.render();
        }, 1000);
        
    }

}

var clock = new Clock({template: 'h:m:s'});
clock.start();