var a = 1;
var z = 20;
var kata = "I love coding";

console.log("No. 1 Looping While");
console.log("LOOPING PERTAMA");
while(a<=z){
    if(a%2==0){
        console.log(a+" - "+kata);
    }
    a++;
}

a=1;
z=20;
kata="I will become a mobile developer";
console.log("LOOPING KEDUA");
while(z>=a){
    if(z%2==0){
        console.log(z+" - "+kata);
    }
    z--;
}

console.log("");
console.log("No. 2 Looping For");
a=1;
z=20;
kata="";
for(a; a<=z; a++){
    if(a%2==1){
        if(a%3==0){
            console.log(a+" - I Love Coding");
        }else{
            console.log(a+" - Santai");
        }
    }else{
        console.log(a+" - Berkualitas");
    }
}

console.log("");
console.log("No. 3 Membuat Persegi Panjang #");
var tag = "########";
for(a=0; a<4; a++){
    console.log(tag);
}

console.log("");
console.log("No. 4 Membuat Tangga #");
tag = "";
for(a=0; a<7; a++){
    for(b=0; b<=0; b++){
        tag=tag+"#";
    }
    console.log(tag);
}

console.log("");
console.log("No. 5 Membuat Papan Catur #");
tag = " # # # #";
tag2 = "# # # # ";
for(a=0; a<8; a++){
    if(a%2==0){
        console.log(tag);
    }else{
        console.log(tag2);
    }
    
}