console.log("=== Soal No. 1 (Range) ===");
function range (startNum, finishNum){
    if(startNum==undefined||finishNum==undefined){
        return -1;
    }else{
        var number=[];
        if(startNum<finishNum){
            for (startNum; startNum<=finishNum; startNum++){
                number.push(startNum);
            }
        }else{
            for (startNum; startNum>=finishNum; startNum--){
                number.push(startNum);
            }
        }
        return number;
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("=== Soal No. 2 (Range with Step) ===");
function rangeWithStep (startNum, finishNum, step){
    var number=[];
    if(startNum<finishNum){
        for (startNum; startNum<=finishNum; startNum++){
            number.push(startNum);
            startNum=startNum+step-1;
        }
    }else{
        for (startNum; startNum>=finishNum; startNum--){
            number.push(startNum);
            startNum=startNum-step+1;
        }
    }
    return number;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("=== Soal No. 3 (Sum of Range) ===");
function sum (startNum, finishNum, step){
    var ret=0;
    if (step==undefined){
        step=1;
    }
    if(startNum==undefined || finishNum==undefined){
        if(startNum!=undefined){
            ret=startNum;
        }else{
            ret=0;
        }
    }else{
        var number=[];
        var angka = 0;
        if(startNum<finishNum){
            for (startNum; startNum<=finishNum; startNum++){
                number.push(startNum);
                startNum=startNum+step-1;
                ret=ret+number[angka];
                angka++;
            }
        }else{
            for (startNum; startNum>=finishNum; startNum--){
                number.push(startNum);
                startNum=startNum-step+1;
                ret=ret+number[angka];
                angka++;
            }
        }
    }
    return ret;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("=== Soal No. 4 (Array Multidimensi) ===");
function dataHandling (input){
    for (a=0; a<input.length; a++){
        var z = input[a];
        for (b=0; b<z.length; b++){
            var kata;
            if(b==0){
                kata="Nomor ID \t: "+z[b];
                console.log(kata);
            }else if(b==1){
                kata="Nama Lengkap\t: "+z[b];
                console.log(kata);
            }else if(b==2){
                kata="TTL\t\t: "+z[b]+", "+z[b+1];
                console.log(kata);
            }else if(b==4){
                kata="Hobi\t\t: "+z[b];
                console.log(kata);
            }
        }
        console.log();
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;
dataHandling(input);

console.log("=== Soal No. 5 (Balik Kata) ===");
function balikKata(a){
    var kata="";
    var inc = a.length;
    for (z=0; z<a.length; z++){
        inc--;
        kata=kata+a[inc];
    }
    return kata;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("=== Soal No. 6 (Metode Array) ===")
function dataHandling2 (a){
    a.splice(1, 0, "Roman Alamsyah Elsharawy");
    a.splice(2, 2, "Provinsi Bandar Lampung");
    a.splice(4, 0, "Pria");
    a.splice(5, 1, "SMA Internasional Metro");
    console.log(a);
    namabulan = "";
    var bulan = a[3].split("/");
    switch (bulan[1]){
        case "01": 
            namabulan = "Januari";
        break;
        case "02": {
            namabulan = "Februari";
            break;
        }
        case "03": {
            namabulan = "Maret";
            break;
        }
        case "04": {
            namabulan = "April";
            break;
        }
        case "05": {
            namabulan = "Mei";
            break;
        }
        case "06": {
            namabulan = "Juni";
            break;
        }
        case "07": {
            namabulan = "Juli";
            break;
        }
        case "08": {
            namabulan = "Agustus";
            break;
        }
        case "09": {
            namabulan = "September";
            break;
        }
        case "10": {
            namabulan = "Oktober";
            break;
        }     
        case "11": {
            namabulan = "November";
            break;
        }
        case "12": { 
            namabulan = "Desember";
            break;
        }
        default : {
            namabulan = "Invalid";
        }
    }
    console.log(namabulan);
    var bl2 = a[3].split("/");
    bl2.sort(function (a, b) { return b - a } );
    console.log(bl2);

    var slug = a[3].split("/");
    console.log(slug.join("-"));

    var sl = a[1];
    console.log(sl.slice(0,14));

}



var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 