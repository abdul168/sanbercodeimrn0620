var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var waktu = 10000;
var a=0;
function askReadBook(){
    readBooksPromise(waktu, books[a])
    .then (function (result) {
        waktu = result;
        a++;
        if(a<books.length){
            askReadBook();
        }
    })
    .catch (function (error){
        console.log("err = "+error);
    })

}
askReadBook();