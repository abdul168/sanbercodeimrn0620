import React from 'react';
import { StyleSheet, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App'

export default function App() {
  return (
    <YoutubeUI/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
