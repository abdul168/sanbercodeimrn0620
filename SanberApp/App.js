import React from 'react';
import { StyleSheet, View } from 'react-native';

import Login from './Tugas/Tugas13/LoginScreen';
import Register from './Tugas/Tugas13/Register';
import About from './Tugas/Tugas13/About';
import Apptodo from './Tugas/Tugas14/App';
import NavigationExample from './Tugas/Tugas15/index';
import TugasNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index'

export default function App() {
  return (
    <Quiz3 />
    // <TugasNavigation/>
    // <NavigationExample/>
    // <Apptodo/>
    // <Login/>
    // <Register/>
    // <About/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});