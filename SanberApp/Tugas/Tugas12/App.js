import React from 'react';
import { View,StyleSheet,Image, TouchableOpacity,Text, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './Components/videoitem';
import Data from './data.json';

export default class App extends React.Component {
    render(){
        return (
          <View style={styles.container}>
            <View style={styles.navBar}>
              <Image source={require('./images/logo.png')} style={{width:98, height:22}} />
              <View style={styles.rightNav}>
                <TouchableOpacity>
                  <Icon style={styles.navItem} name="search" size={25}/>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Icon style={styles.navItem} name="account-circle" size={25}/>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.body}>
              
              <FlatList
                data={Data.items}
                renderItem={(video)=><VideoItem video={video.item}/>}
                keyExtractor={(item)=>item.id}
                ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor:"#E5E5E5"}}/>}
              />
            </View>
            <View style={styles.tabBar}>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="home" size={25}/>
                <Text style={styles.tabTitel}>Home</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="whatshot" size={25}/>
                <Text style={styles.tabTitel}>Trending</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="subscriptions" size={25}/>
                <Text style={styles.tabTitel}>Subscriptions</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="folder" size={25}/>
                <Text style={styles.tabTitel}>Library</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
    }
}


const styles=StyleSheet.create({
    container:{
        flex:1,
       

    },
    navBar:{
      height:55,
      backgroundColor:'white',
      elevation:3,
      paddingHorizontal:15,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'space-between',
      marginTop:20

    // borderColor:'black',
    // borderRadius:3
    },
    rightNav:{
        flexDirection:'row',
    },
    navItem:{
        marginLeft:20
    },
    body:{
        flex:1
    },
    tabBar:{
        backgroundColor:'white',
        height:60,
        borderWidth:0.5,
        borderColor:'#E5E5E5',
        flexDirection:'row',
        justifyContent:'space-around'
    },
    tabItem:{
        alignItems:'center',
        justifyContent:'center'
    },
    tabTitel:{
        fontSize:11,
        color:'#3c3c3c'
    }


});