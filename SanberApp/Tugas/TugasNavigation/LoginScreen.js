import React from 'react';
import { 
    View,
    StyleSheet,
    Image,
    Text,
    TouchableOpacity, 
    TextInput
} from 'react-native';

export default function Login( {navigation} ) {
    return (
        <View style={styles.container}>
            <View style={styles.logoBar}>
                <Image source={require('./images/logo.png')} style={{width:300, height:100}} />
            </View>
            <View style={styles.textBar}>
                <Text style={{fontSize:20, fontWeight : 'bold', color : '#003366'}}>LOGIN</Text>
            </View>
            <Text style={{fontSize:15, paddingTop : 20, paddingLeft:15, color : '#003366'}}>Username / Email</Text>
            <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Email"
                placeholderTextColor = "#3EC6FF"
                autoCapitalize = "none"/>
            <Text style={{fontSize:15, paddingLeft:15, color : '#003366'}}>Password</Text>
            <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Password"
                placeholderTextColor = "#3EC6FF"
                autoCapitalize = "none"/>
            <View style={styles.textBar}>
                <TouchableOpacity backgroundColor
                    style = {styles.submitButton}
                    onPress={ () => navigation.navigate('DrawerStackScreen') } >
                    <Text style = {styles.submitButtonText}> Masuk </Text>
                </TouchableOpacity>
                <Text style={{fontSize:18, fontWeight : 'bold', color : '#3EC6FF'}}>atau</Text>
                <TouchableOpacity
                    style = {styles.submitButton2}
                    onPress={ () => navigation.navigate('RegisterScreen') } >
                    <Text style = {styles.submitButtonText}> Daftar ? </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'white'
    },
    logoBar: {
       alignItems : 'center',
       height:140,
       marginTop:60
    },
    textBar: {
        alignItems : 'center'
    },
    input: {
        marginTop : 4,
        marginBottom: 12,
        marginLeft:15,
        marginRight:15,
        paddingLeft:15,
        paddingRight:15,
        height: 40,
        borderColor: '#003366',
        borderWidth: 1,
        borderRadius : 5
     },
     submitButton: {
        backgroundColor: '#003366',
        padding: 10,
        margin: 15,
        width : 100,
        borderRadius : 10,
        alignItems : 'center'
     },
     submitButton2: {
        backgroundColor: '#3EC6FF',
        padding: 10,
        margin: 15,
        width : 100,
        borderRadius : 10,
        alignItems : 'center'
     },
     submitButtonText:{
        color: 'white',
        fontSize : 18,
        fontWeight : 'bold'
     }
  });