import React from 'react';
import { 
    View,
    StyleSheet,
    Text
} from 'react-native';
export const ProjectScreen = ({navigation}) => {
    return(
        <View style={styles.container}>
            <Text style={styles.text}>Project Screen</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center'

      },
    text:{
        color: '#000000',
        fontSize : 28,
        fontWeight : 'bold',
        justifyContent: "space-around",
        marginTop:250
     }

});