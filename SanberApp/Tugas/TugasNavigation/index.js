import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from "@react-navigation/drawer";
import {SkillScreen} from "./SkillScreen";
import {ProjectScreen} from "./ProjectScreen";
import {AddScreen} from "./AddScreen";
import {AboutScreen} from "./AboutScreen";
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';


import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {AuthContext} from './context';

const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();
const RootStack = createStackNavigator();

const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();

const iconTab = ({ route }) => {
  return ({
      tabBarIcon: ({ focused, color, size }) => {
          if (route.name == 'SkillScreen') {
              return <FontAwesome5 name='cogs' size={size} color={color} solid />
          } else if (route.name == 'ProjectScreen') {
              return <FontAwesome5 name="file" size={size} color={color} solid />
          } else if (route.name == 'AddScreen') {
              return <FontAwesome5 name="plus-circle" size={size} color={color} solid />
          }
      },
  });
}

const TabsStackScreen = () => (
  <TabsStack.Navigator screenOptions={iconTab} >
      <TabsStack.Screen name='SkillScreen' component={SkillScreen}
          options={{
              title: 'Skill'
          }} />
      <TabsStack.Screen name='ProjectScreen' component={ProjectScreen}
          options={{
              title: 'Project'
          }} />
      <TabsStack.Screen name='AddScreen' component={AddScreen}
          options={{
              title: 'Add'
          }}
      />
  </TabsStack.Navigator>
);

const DrawerStackScreen = () => (
  <DrawerStack.Navigator >
      <DrawerStack.Screen name='TabsStackScreen' component={TabsStackScreen}
          options={{
              title: 'Portofolio'
          }} />
      <DrawerStack.Screen name='AboutScreen' component={AboutScreen}
          options={{
              title: 'About'
          }}
      />
  </DrawerStack.Navigator>
);

const RootStackScreen = () => (
  <RootStack.Navigator>
      <RootStack.Screen name='LoginScreen' component={LoginScreen}
          options={{
              title: 'Login'
          }}
      />
      <RootStack.Screen name='RegisterScreen' component={RegisterScreen}
          options={{
              title: 'Register'
          }}
      />
      <RootStack.Screen name='DrawerStackScreen' component={DrawerStackScreen}
          options={{
              title: 'HOME'
          }}
      />
  </RootStack.Navigator>
);

export default () => (
  <NavigationContainer>
    <RootStackScreen />
  </NavigationContainer>
)