import React from 'react';
import { 
    View,
    StyleSheet,
    Image,
    Text,
    TouchableOpacity, 
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Data from './data.json';

export const AboutScreen = ({navigation}) => {
    // render () {
        return (
            <View style={styles.container}>
                <View style={styles.textBar}>
                    <Text style={{fontSize:30, fontWeight : 'bold', color : '#003366'}}>About Me</Text>
                </View>
                <View style={styles.logoBar}>
                    <Icon name="account-circle" size={180} color='#3EC6FF'/>
                </View>
                <View style={{alignItems : 'center', marginBottom:5}}>
                    <Text style={{fontSize:25, fontWeight : 'bold', color : '#003366'}}>{Data.data[0].username}</Text>
                    <Text style={{fontSize:15, fontWeight : 'bold', color : '#3EC6FF'}}>{Data.data[0].job}</Text>
                </View>
                <View style={styles.flexBar}>
                    <Text style={{fontSize:18, color : '#003366'}}>Protofolio</Text>
                    <View style={{borderBottomColor: '#003366',borderBottomWidth: 1,}}/>
                    <View style={{flexDirection:'row', justifyContent:'space-around', marginTop:10}}>
                        <TouchableOpacity style={{alignItems:'center', justifyContent:'center'}}>
                            <FontAwesome5 name="gitlab" size={30} color='#3EC6FF'/>
                            <Text style={{marginTop:5, fontSize:15, fontWeight : 'bold', color : '#003366'}}>@kang-ketik</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{alignItems:'center', justifyContent:'center'}}>
                            <FontAwesome5 name="github" size={30} color='#3EC6FF'/>
                            <Text style={{marginTop:5, fontSize:15, fontWeight : 'bold', color : '#003366'}}>@copas-aja</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.flexBar}>
                    <Text style={{fontSize:18, color : '#003366'}}>Contact Us</Text>
                    <View style={{borderBottomColor: '#003366',borderBottomWidth: 1,}}/>
                    <View style={{marginLeft:80, marginTop:10, alignItems:'flex-start'}}>
                        <TouchableOpacity style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                            <FontAwesome5 name="facebook-square" size={30} color='#3EC6FF'/>
                            <Text style={{marginLeft:15, fontSize:15, fontWeight : 'bold', color : '#003366'}}>bukan_suami_bcl</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginTop:10, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                            <FontAwesome5 name="instagram" size={30} color='#3EC6FF'/>
                            <Text style={{marginLeft:15, fontSize:15, fontWeight : 'bold', color : '#003366'}}>@pacarnya_ariel_tatum</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginTop:10, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                            <FontAwesome5 name="twitter" size={30} color='#3EC6FF'/>
                            <Text style={{marginLeft:15, fontSize:15, fontWeight : 'bold', color : '#003366'}}>@GA_PUNYA</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    // } 
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    logoBar: {
       alignItems : 'center'
    },
    textBar: {
        alignItems : 'center',
        marginTop:20
    },
    flexBar: {
        // alignItems : 'center',
        marginTop:10,
        backgroundColor:'#EFEFEF',
        marginLeft:10,
        marginRight:10,
        paddingTop : 5,
        paddingBottom : 10,
        paddingLeft:15,
        paddingRight:15,
        borderRadius : 5
    }
  });