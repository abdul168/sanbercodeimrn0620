import React from 'react';
import { 
    View,
    StyleSheet,
    Image,
    Text,
    TouchableOpacity, 
    TextInput,
    ScrollView
} from 'react-native';

export default class Register extends React.Component {
    state = {
        username: '',
        email: '',
        password: '',
        password2: ''
     }
     handleUsername = (text) => {
        this.setState({ username: text })
     }
     handleEmail = (text) => {
        this.setState({ email: text })
     }
     handlePassword = (text) => {
        this.setState({ password: text })
     }
     handlePassword2 = (text) => {
        this.setState({ password2: text })
     }
     daftar = (username, email, password, password2) => {
        alert('username: ' + username +'email: ' + email + ' password: ' + password+ ' password2: ' + password2)
     }
    render () {
        return (
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.logoBar}>
                    <Image source={require('./images/logo.png')} style={{width:300, height:100}} />
                </View>
                <View style={styles.textBar}>
                    <Text style={{fontSize:20, fontWeight : 'bold', color : '#003366'}}>REGISTRASI</Text>
                </View>
                <Text style={{fontSize:15, paddingTop : 20, paddingLeft:15, color : '#003366'}}>Username</Text>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Username"
                    placeholderTextColor = "#3EC6FF"
                    autoCapitalize = "none"
                    autoCompleteType = 'username'
                    onChangeText = {this.handleUsername}/>
                <Text style={{fontSize:15, paddingLeft:15, color : '#003366'}}>Email</Text>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Email"
                    placeholderTextColor = "#3EC6FF"
                    autoCapitalize = "none"
                    autoCompleteType = 'email'
                    onChangeText = {this.handleEmail}/>
                <Text style={{fontSize:15, paddingLeft:15, color : '#003366'}}>Password</Text>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Password"
                    placeholderTextColor = "#3EC6FF"
                    autoCapitalize = "none"
                    autoCompleteType = 'password'
                    onChangeText = {this.handlePassword}/>
                <Text style={{fontSize:15, paddingLeft:15, color : '#003366'}}>Ulangi Password</Text>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Ulangi Password"
                    placeholderTextColor = "#3EC6FF"
                    autoCapitalize = "none"
                    autoCompleteType = 'password'
                    onChangeText = {this.handlePassword2}/>
                <View style={styles.textBar}>
                    <TouchableOpacity
                        style = {styles.submitButton1}
                        onPress = {
                            () => this.daftar(this.state.username, this.state.email, this.state.password, this.state.password2)
                        }>
                        <Text style = {styles.submitButtonText}> Daftar </Text>
                    </TouchableOpacity>
                    <Text style={{fontSize:18, fontWeight : 'bold', color : '#3EC6FF'}}>atau</Text>
                    <TouchableOpacity
                        style = {styles.submitButton2}
                        // onPress = {
                        //     () => this.login(this.state.email, this.state.password)
                        // }
                        >
                        <Text style = {styles.submitButtonText}> Masuk ? </Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
        )
    } 
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    logoBar: {
       alignItems : 'center',
       height:140,
       marginTop:60
    },
    textBar: {
        alignItems : 'center'
    },
    input: {
        marginTop : 4,
        marginBottom: 12,
        marginLeft:15,
        marginRight:15,
        paddingLeft:15,
        paddingRight:15,
        height: 40,
        borderColor: '#003366',
        borderWidth: 1,
        borderRadius : 5
     },
     submitButton1: {
        backgroundColor: '#003366',
        padding: 10,
        margin: 15,
        width : 100,
        borderRadius : 10,
        alignItems : 'center'
     },
     submitButton2: {
        backgroundColor: '#3EC6FF',
        padding: 10,
        margin: 15,
        width : 100,
        borderRadius : 10,
        alignItems : 'center'
     },
     submitButtonText:{
        color: 'white',
        fontSize : 18,
        fontWeight : 'bold'
     }
  });