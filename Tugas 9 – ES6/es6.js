golden = () => {
    console.log("this is golden!!")
}
   
golden();

newFunction = (firstName, lastName) =>{
    return {
      fullName: function(){
        console.log(`${firstName} ${lastName}`);
        return 
      }
    }
  }
   
newFunction("William", "Imoh").fullName()

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// const combined = west.concat(east)
const combined = [...west, ...east];
console.log(combined)

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + `${planet}` + ' dolor sit amet, ' +  
    'consectetur adipiscing elit,' + `${view}` + ' do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before) 