console.log("=== 1. SOAL CLASS SCORE ===");
class Score {
    constructor (subject, points, email) {
        this.email = email;
        this.subject=subject;
        var point=0
        if(points.length != undefined){
            var i = 0, sum = 0, len = points.length;
            while (i < len) {
                sum = sum + points[i++];
            }
            point=sum / len;
        }else{
            point=points;
        }
        this.points=point;
        return ( this.email, this.subject, this.points);
    }
    
}

var call = new Score("abdul", [10,24,35,47], "abdul@gmail.com");
var call2 = new Score("latif", 23, "latif@gmail.com");

console.log(call);
console.log(call2);

console.log("");
console.log("=== 2. SOAL Create Score ===");
function viewScores(data, subject) {
    var quiz=0;
    if(subject=='quiz-1'){
        console.log(subject);
        quiz=1;
    }else if (subject=='quiz-2'){
        console.log(subject);
        quiz=2;
    }else if (subject=='quiz-3'){
        console.log(subject);
        quiz=3;
    }else{
        console.log("Quiz Tidak Tersedia");
        return;
    }
    var dt ;
    
    for (a=1; a<data.length; a++){
        dt=data[a];
        if(quiz==1){
            var c1 = new Score(subject, dt[1], dt[0]);
            console.log(c1);
        }else if (quiz==2){
            var c2 = new Score(subject, dt[2], dt[0]);
            console.log(c2);
        }else if (quiz==3){
            var c3 = new Score(subject, dt[3], dt[0]);
            console.log(c3);
        }
    }
}
// TEST CASE
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


console.log("");
console.log("=== 3. SOAL Recap Score ===");

function recapScores(data) {
    var dt2;
    for (a=1; a<data.length; a++){
        dt2=data[a];
        var c = new Score("", [dt2[1],dt2[2],dt2[3]], dt2[0]);
        console.log(a+". "+"E-mail : "+c.email);
        console.log("Rata-Rata : "+c.points);
        if(c.points>70 && c.points<=80){
            console.log("Predikat : participant");
        }else if(c.points>80 && c.points<=90){
            console.log("Predikat : graduate");
        }else if(c.points>90){
            console.log("Predikat : honour");
        }
    }
}

recapScores(data);