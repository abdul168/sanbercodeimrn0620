console.log("=== A. String Terbalik ===")
function balikKata(a){
    let kata="";
    let inc = a.length;
    for (z=0; z<a.length; z++){
        inc--;
        kata=kata+a[inc];
    }
    return kata;
}

// TEST CASES String Terbalik
console.log(balikKata("abcde")) // edcba
console.log(balikKata("rusak")) // kasur
console.log(balikKata("racecar")) // racecar
console.log(balikKata("haji")) // ijah

console.log();
console.log("=== B. Bandingkan Angka ===")
function maksimum(a, b){
    let mak = "";
    if (a<0){
        mak=-1;
        return mak;
    }else if (a==b){
        mak=-1;
        return mak;
    }else if (a < b){
        mak=b;
        return mak;
    }else if (a == undefined){
        mak=-1;
        return mak;
    }else if (b == undefined){
        mak=1;
        return mak;
    }
}

console.log(maksimum(10, 15)); // 15
console.log(maksimum(12, 12)); // -1
console.log(maksimum(-1, 10)); // -1 
console.log(maksimum(112, 121));// 121
console.log(maksimum(1)); // 1
console.log(maksimum()); // -1
console.log(maksimum("15", "18")) // 18

console.log();
console.log("=== C. Palindrome ===")
function palindrome(a){
    let bol2 = false;
    let kata2="";
    let inc2 = a.length;
    for (z=0; z<a.length; z++){
        inc2--;
        kata2=kata2+a[inc2];
    }
    if(kata2==a){
        bol2=true;
    }
    return bol2;
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false