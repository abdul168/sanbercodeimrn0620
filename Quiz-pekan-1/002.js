
console.log("=== A. Descending Ten ===")
function DescendingTen (a){
    if(a!=undefined){
        let contc = "";
        let x = a-10;
        for (a; a>x; a--){
            contc = contc+a+" ";
        }
        return contc;
    }else{
        return -1;
    }
    
}
// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log();
console.log("=== B. Ascending Ten ===")
function AscendingTen (a){
    if(a!=undefined){
        let contc2 = "";
        let x2 = a+10;
        for (a; a<x2; a++){
            contc2 = contc2+a+" ";
        }
        return contc2;
    }else{
        return -1;
    }
    
}

// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log();
console.log("=== C. Conditional Ascending Descending ===")
function ConditionalAscDesc (a, b){
    if(a==undefined || b==undefined){
        return -1;
    }else if (a%2==0){
        let contc3 = "";
        let x3 = a-10;
        for (a; a>x3; a--){
            contc3 = contc3+a+" ";
        }
        return contc3;
    }else{
        let contc4 = "";
        let x4 = a+10;
        for (a; a<x4; a++){
            contc4 = contc4+a+" ";
        }
        return contc4;

    }
    
}
// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1